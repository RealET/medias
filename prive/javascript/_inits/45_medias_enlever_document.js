// animation qui précéde le retrait des documents liés via boutons d'action
if (!Object.hasOwn(window,'medias_enlever_document')) { 
	//rendre accessible globalement
	window.medias_enlever_document = async (doc) => {
		return await animateRemove(document.getElementById(doc));
	}
}
