<?php

/**
 * Retourne le logo d'un objet, éventuellement par héritage
 *
 * Si flag != false, retourne le chemin du fichier, sinon retourne un tableau
 * de 3 elements :
 * le chemin du fichier, celui du logo de survol, l'attribut style=w/h.
 *
 * @param string $cle_objet
 *     Nom de la clé de l'objet dont on veut chercher le logo.
 * @param string $onoff
 *     Sélectionne quel(s) logo(s) : "on" pour le logo normal, "off" pour le logo de survol, ou "ON" pour l'ensemble.
 * @param int $id
 *     Identifiant de l'objet dont on veut chercher le logo.
 * @param int $id_rubrique
 *     Identifiant de la rubrique parente si l'on veut aller chercher son logo
 *     dans le cas où l'objet demandé n'en a pas.
 * @deprecated @param bool $flag
 *     Lorsque le drapeau est évalué comme "true", la fonction ne renvoie
 *     que le chemin du fichier, sinon elle renvoie le tableau plus complet.
 * @return array|string
 *     Retourne soit un tableau, soit le chemin du fichier.
 */
function quete_logo($cle_objet, $onoff, $id, $id_rubrique, $flag = false) {
	include_spip('base/objets');
	$nom = strtolower($onoff);

	$cle_objet = id_table_objet($cle_objet);

	while (1) {
		$objet = objet_type($cle_objet);

		$on = quete_logo_objet($id, $objet, $nom);

		if ($on) {
			if ($flag) {
				return $on['fichier'];
			}
			$taille = @spip_getimagesize($on['chemin']);

			// Si on a déjà demandé un survol directement ($onoff = off)
			// ou qu'on a demandé uniquement le normal ($onoff = on)
			// alors on ne cherche pas du tout le survol ici
			$off = $onoff != 'ON' ? '' : quete_logo_objet($id, $objet, 'off');

			// on retourne une url du type IMG/artonXX?timestamp
			// qui permet de distinguer le changement de logo
			// et placer un expire sur le dossier IMG/
			$res = [
				$on['chemin'] . ($on['timestamp'] ? "?{$on['timestamp']}" : ''),
				($off ? $off['chemin'] . ($off['timestamp'] ? "?{$off['timestamp']}" : '') : ''),
				($taille ? ' ' . $taille[3] : ('')),
			];
			$res['src'] = $res[0];
			$res['logo_on'] = $res[0];
			$res['logo_off'] = $res[1];
			$res['width'] = ($taille ? $taille[0] : '');
			$res['height'] = ($taille ? $taille[1] : '');
			$res['fichier'] = $on['fichier'];
			$res['titre'] = ($on['titre'] ?? '');
			$res['descriptif'] = ($on['descriptif'] ?? '');
			$res['credits'] = ($on['credits'] ?? '');
			$res['alt'] = ($on['alt'] ?? '');
			$res['id'] = ($on['id_document'] ?? 0);

			return $res;

		}
		if (defined('_LOGO_RUBRIQUE_DESACTIVER_HERITAGE')) {
			return '';
		}
		if ($id_rubrique) {
			$cle_objet = 'id_rubrique';
			$id = $id_rubrique;
			$id_rubrique = 0;
		} else {
			if ($id && $cle_objet == 'id_rubrique') {
				$id = quete_parent($id);
			} else {
				return '';
			}
		}

	}
}

/**
 * Chercher le logo d'un contenu précis
 *
 * @param int $id_objet
 * 		Idenfiant de l'objet dont on cherche le logo
 * @param string $objet
 * 		Type de l'objet dont on cherche le logo
 * @param string $mode
 * 		"on" ou "off" suivant le logo normal ou survol
 * @return bool|array
 */
function quete_logo_objet($id_objet, $objet, $mode) {
	static $chercher_logo;
	if ($chercher_logo === null) {
		$chercher_logo = charger_fonction('chercher_logo', 'inc');
	}
	$cle_objet = id_table_objet($objet);

	// On cherche pas la méthode classique
	$infos_logo = $chercher_logo((int) $id_objet, $cle_objet, $mode);

	// Si la méthode classique a trouvé quelque chose, on utilise le nouveau format
	if (!empty($infos_logo)) {
		$infos = [
			'chemin' => $infos_logo[0],
			'timestamp' => $infos_logo[4],
			'id_document' => ($infos_logo[5]['id_document'] ?? ''),
		];
		foreach (['fichier', 'titre', 'descriptif', 'credits', 'alt'] as $champ) {
			$infos[$champ] = ($infos_logo[5][$champ] ?? '');
		}
		$infos_logo = $infos;
	}

	// On passe cette recherche de logo dans un pipeline
	$infos_logo = pipeline(
		'quete_logo_objet',
		[
			'args' => [
				'id_objet' => $id_objet,
				'objet' => $objet,
				'cle_objet' => $cle_objet,
				'mode' => $mode,
			],
			'data' => $infos_logo,
		]
	);

	return $infos_logo;
}

/**
 * Retourne le logo d’un fichier (document spip) sinon la vignette du type du fichier
 *
 * Fonction appeleé par la balise `#LOGO_DOCUMENT`
 *
 * @param array $row
 * @param string $connect
 * @return bool|string
 */
function quete_logo_file($row, $connect = null) {
	include_spip('inc/documents');
	$logo = vignette_logo_document($row, $connect);
	if (!$logo) {
		$logo = image_du_document($row, $connect);
	}
	if (!$logo) {
		$f = charger_fonction('vignette', 'inc');
		$logo = $f($row['extension'], $row['media']);
	}
	// si c'est une vignette type doc, la renvoyer direct
	if (
		strcmp((string) $logo, (string) _DIR_PLUGINS) == 0
		|| strcmp((string) $logo, (string) _DIR_PLUGINS_DIST) == 0
		|| strcmp((string) $logo, _DIR_RACINE . 'prive/') == 0
	) {
		return $logo;
	}

	return get_spip_doc($logo);
}

/**
 * Trouver l'image logo d'un document
 *
 * @param array $row
 *   description du document, issue de la base
 * @param  $lien
 *   url de lien
 * @param  $align
 *   alignement left/right
 * @param  $mode_logo
 *   mode du logo :
 *     '' => automatique (vignette sinon apercu sinon icone)
 *     icone => icone du type du fichier
 *     apercu => apercu de l'image exclusivement, meme si une vignette existe
 *     vignette => vignette exclusivement, ou rien si elle n'existe pas
 * @param  $x
 *   largeur maxi
 * @param  $y
 *   hauteur maxi
 * @param string $connect
 *   serveur
 * @return string
 */
function quete_logo_document($row, $lien, $align, $mode_logo, $x, $y, string $connect = '') {

	include_spip('inc/documents');
	$logo = '';
	if (!in_array($mode_logo, ['icone', 'apercu'])) {
		$logo = vignette_logo_document($row, $connect);
	}
	// si on veut explicitement la vignette, ne rien renvoyer si il n'y en a pas
	if ($mode_logo == 'vignette' && !$logo) {
		return '';
	}
	if ($mode_logo == 'icone') {
		$row['fichier'] = '';
	}

	return vignette_automatique($logo, $row, $lien, $x, $y, $align, null, $connect);
}

/**
 * Recuperer le HTML du logo d'apres ses infos
 * @param array $logo
 * @param string $align
 * @param string $lien
 * @return string
 */
function quete_html_logo($logo, $align, $lien) {

	if (!is_array($logo)) {
		return '';
	}

	$contexte = [];
	foreach ($logo as $k => $v) {
		if (!is_numeric($k)) {
			$contexte[$k] = $v;
		}
	}

	foreach (['titre', 'descriptif', 'credits', 'alt'] as $champ) {
		if (!empty($contexte[$champ])) {
			$contexte[$champ] = appliquer_traitement_champ($contexte[$champ], $champ, 'document');
		}
	}

	$contexte['align'] = $align;
	$contexte['lien'] = $lien;
	return recuperer_fond('modeles/logo', $contexte);
}

/**
 * Retourne le chemin d’un document lorsque le connect est précisé
 *
 * Sur un connecteur distant, voir si on connait l’adresse du site (spip distant)
 * et l’utiliser le cas échéant.
 *
 * @param string $fichier Chemin
 * @param string $connect Nom du connecteur
 * @return string|false
 */
function document_spip_externe($fichier, $connect) {
	if ($connect) {
		$site = quete_meta('adresse_site', $connect);
		if ($site) {
			$dir = quete_meta('dir_img', $connect);
			return "$site/$dir$fichier";
		}
	}
	return false;
}

/**
 * Retourne la vignette explicitement attachee a un document
 * le resutat est un fichier local existant, ou une URL
 * ou vide si pas de vignette
 *
 * @param array $row
 * @return string
 */
function vignette_logo_document($row, string $connect = '') {

	if (!$row || empty($row['id_vignette'])) {
		return '';
	}
	$fichier = quete_fichier($row['id_vignette'], $connect);
	if ($url = document_spip_externe($fichier, $connect)) {
		return $url;
	}

	$f = get_spip_doc($fichier);
	if ($f && @file_exists($f)) {
		return $f;
	}
	if ($row['mode'] !== 'vignette') {
		return '';
	}

	return generer_objet_url($row['id_document'], 'document', '', '', null, '', $connect);
}

/**
 * Retourne le fichier d'un document
 *
 * @param int $id_document
 * @param string $serveur
 * @return array|bool|null
 */
function quete_fichier($id_document, $serveur = '') {
	return sql_getfetsel('fichier', 'spip_documents', ('id_document=' . (int) $id_document), '', [], '', '', $serveur);
}

/**
 * Toute les infos sur un document
 *
 * @param int $id_document
 * @param string $serveur
 * @return array|bool
 */
function quete_document($id_document, $serveur = '') {
	return sql_fetsel('*', 'spip_documents', ('id_document=' . (int) $id_document), '', [], '', '', $serveur);
}
